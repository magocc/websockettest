package com.mago.websockettest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import ua.naiksoftware.stomp.Stomp
import ua.naiksoftware.stomp.StompClient
import ua.naiksoftware.stomp.dto.StompHeader

class MainActivity : AppCompatActivity() {

    lateinit var mStompClient: StompClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.mago.websockettest.R.layout.activity_main)

        conectar.setOnClickListener {
            mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, "https://laipdemobile.net/all-mobile-flanders/public/websocket-all-mobile")

            val headers = listOf(StompHeader("username", "vprelliasco"), StompHeader("origen", "android"))
            mStompClient.connect(headers)

            mStompClient.topic("/api/websocket/listener/AR/planificacion/108").subscribe {
                Log.d("Websockets", it.payload)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mStompClient.disconnect()
    }
}